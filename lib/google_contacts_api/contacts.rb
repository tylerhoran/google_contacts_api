# Module that implements a method to get contacts for a user or group
module GoogleContactsApi
  module Contacts
    # Retrieve the contacts for this user or group
    def get_contacts(params = {})
      # TODO: Should return empty ContactSet (haven't implemented one yet)
      return [] unless @api
      params = params.with_indifferent_access

      # compose params into a string
      # See http://code.google.com/apis/contacts/docs/3.0/reference.html#Parameters
      # alt, q, max-results, start-index, updated-min,
      # orderby, showdeleted, requirealldeleted, sortorder, group
      params["max-results"] = 100000 unless params.key?("max-results")
      url = "contacts/default/full"
      response = @api.get(url, params)

      # TODO: Define some fancy exceptions
      case GoogleContactsApi::Api.parse_response_code(response)
      when 401; raise
      when 403; raise
      when 404; raise
      when 400...500; raise
      when 500...600; raise
      end
      GoogleContactsApi::ContactSet.new(response.body, @api)
    end

    def create_contact(params)
      response = @api.post('contacts/default/full', contact_xml(params), {},
                          'Content-Type' => 'application/atom+xml')
      Contact.new(Hashie::Mash.new(JSON.parse(response.body)).entry)
    end

    def update_contact(contact, params)
      response = @api.put(contact.id_path, contact_xml(params), {},
                          {'Content-Type' => 'application/atom+xml',
                          'If-Match' => contact.etag })
      Contact.new(Hashie::Mash.new(JSON.parse(response.body)).entry)
    end

    def delete_contact(contact)
      @api.delete(contact.id_path, {}, 'If-Match' => contact.etag)
    end

    private

    def contact_xml(params)
      <<-EOS
      <atom:entry xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:gd="http://schemas.google.com/g/2005">
        <atom:category scheme="http://schemas.google.com/g/2005#kind"
          term="http://schemas.google.com/contact/2008#contact"/>
        <gd:name>
           <gd:givenName>#{params[:given_name]}</gd:givenName>
           <gd:familyName>#{params[:family_name]}</gd:familyName>
           <gd:fullName>#{params[:full_name]}</gd:fullName>
        </gd:name>
        <atom:content type="text">#{params[:notes]}</atom:content>
        <gd:email rel="http://schemas.google.com/g/2005#work"
          primary="true"
          address="#{params[:email]}"/>
        <gd:structuredPostalAddress
            rel="http://schemas.google.com/g/2005#work"
            primary="true">
          <gd:city>#{params[:city]}</gd:city>
          <gd:street>#{params[:address]}</gd:street>
          <gd:region>#{params[:state]}</gd:region>
          <gd:postcode>#{params[:zip]}</gd:postcode>
          <gd:country>#{params[:country]}</gd:country>
          <gd:formattedAddress>
            #{params[:formatted_address]}
          </gd:formattedAddress>
        </gd:structuredPostalAddress>
      </atom:entry>
      EOS
    end
  end
end
